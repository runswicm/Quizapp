## Login Details

Login for both players is:
- username: admin
- password: 1234

Login button is diabled after 5 incorrect attempts. Must restart app if this occurs.

## Possible Rules for Quiz-App:

 - 10 questions (posibly better score for answering faster)
 - put coins in at the start (can have different tiers of betting)
 - can double the coins at the end of the 10 questions (possibly at 5 questions as well)
 - get coins every hour (enough that you dont run out for now to make testing easy)
 - not showing score to other person
