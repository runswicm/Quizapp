package com.example.triviapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    public static final String EXTRA_MESSAGE = "com.example.TriviApp.MESSAGE";
    public static String PAGE_ID;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.example_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case  R.id.item1:
                Toast.makeText(this, "Item 1 selected", Toast.LENGTH_SHORT).show();
                return true;

            case  R.id.item2:
                Toast.makeText(this, "Item 2 selected", Toast.LENGTH_SHORT).show();
                return true;

            case  R.id.item3:
                Toast.makeText(this, "Item 3 selected", Toast.LENGTH_SHORT).show();
                return true;

            case  R.id.subitem1:
                Toast.makeText(this, "Sub Item 1 selected", Toast.LENGTH_SHORT).show();
                return true;

            case  R.id.subitem2:
                Toast.makeText(this, "Sub Item 2 selected", Toast.LENGTH_SHORT).show();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }

    }

    public void QPage(View view)
    {
        Button myButton = (Button) (findViewById(view.getId()));
        String categoryText = (String) (myButton.getText());
        Intent intent=new Intent(this, Q_Page.class);

        if(categoryText.equals("Game of Thrones")) {
            PAGE_ID = "1";
        }
        else if(categoryText.equals("How I Met Your Mother")) {
            PAGE_ID = "2";
        }
        else if(categoryText.equals("F.R.I.E.N.D.S")) {
            PAGE_ID = "3";
        }
        else if(categoryText.equals("The Office (US)")) {
            PAGE_ID = "4";
        }
        else if(categoryText.equals("The Simpsons")) {
            PAGE_ID = "5";
        }
        else{
            PAGE_ID = "1";
        }

        intent.putExtra(EXTRA_MESSAGE,PAGE_ID);
        startActivity(intent);
    }

}
