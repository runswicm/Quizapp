package com.example.triviapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class launch_screen extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_launch_screen);

        Thread myThread = new Thread() {
            @Override
            public void run() {
                try {
                    int minimum = 1000;
                    int maximum = 3000;
                    double loadtime = minimum + (int)(Math.random() * maximum);
                    loadtime += 1000;
                    sleep((long)loadtime);
                    Intent intent = new Intent(launch_screen.this, login_page.class);
                    startActivity(intent);
                    finish();
                } catch (InterruptedException e) {
                    e.printStackTrace();

                }


            }
        };

    myThread.start();
    }
}
