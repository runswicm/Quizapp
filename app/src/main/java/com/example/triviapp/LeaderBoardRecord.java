package com.example.triviapp;

class LeaderBoardRecord {
    int Rank;
    int Score;
    String Name;

    public int getRank() {
        return Rank;
    }

    public void setRank(int rank) {
        Rank = rank;
    }

    public int getScore() {
        return Score;
    }

    public void setScore(int score) {
        Score = score;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    @Override
    public String toString() {
        return "LeaderBoardRecord{" +
                "Rank=" + Rank +
                ", Score=" + Score +
                ", Name='" + Name + '\'' +
                '}';
    }
}
