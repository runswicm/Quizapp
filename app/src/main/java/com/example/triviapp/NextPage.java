package com.example.triviapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class NextPage extends AppCompatActivity {

    public static String state;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_next_page);

        Intent intent = getIntent();
        String message = intent.getStringExtra(Q_Page.EXTRA_MESSAGE);
        String[] tokens = message.split(","); //sets the delimeter to a ','
        //takes the score and total and parses them into ints stored in token[0-1]
        int Score = Integer.parseInt(tokens[0]);
        int Total = Integer.parseInt(tokens[1]);
        state =tokens[2];

        //creates a textbox location for the final score on the layout display
        TextView Final_Tally = (TextView) findViewById(R.id.textView4);
        //converts the score and total into a single string and outputs them to the display
        String S = Integer.toString(Score);
        String T = Integer.toString(Total);
        String Final = "Score: " + S + " | Total: " + T;
        Final_Tally.setText(Final);
        try {
            readLeaderBoard();
        } catch (IOException e) {
            e.printStackTrace();
        }
        /*LeaderBoardRecord rec1=new LeaderBoardRecord();
        String N="Mee";
        rec1.setRank(4);
        rec1.setScore(Score);
        rec1.setName(N);
        Ranks.add(rec1);*/

        //code to update leaderboard
        //load the stored leaderboard info into rec1,2 and 3
        LeaderBoardRecord rec1;
        LeaderBoardRecord rec2;
        LeaderBoardRecord rec3;
        rec1 = Ranks.get(0);
        rec2 = Ranks.get(1);
        rec3 = Ranks.get(2);

        //check to see if the new score is greater than the smallest score on the leaderboard
        if (rec1.getScore() < Score) { // if its the new best score
            rec3.setScore(rec2.getScore());
            rec3.setName(rec2.getName());

            rec2.setScore(rec1.getScore());
            rec2.setName(rec1.getName());

            rec1.setScore(Score);
            rec1.setName("new");
        } else if (rec2.getScore() < Score) { // if it is the new second best score
            rec3.setScore(rec2.getScore());
            rec3.setName(rec2.getName());

            rec2.setScore(Score);
            rec2.setName("new");
        } else if (rec3.getScore() < Score) { // if it is the new third best score
            rec3.setScore(Score);
            rec3.setName("new");
        }

        //store these updated names and scores in the csv file
        //create an instance of a file writer

        try {

            String File_From;
            if(state.equals("1"))
                File_From="leaders.csv";
            else if(state.equals("2"))
                File_From="leadershimym.csv";
            else if(state.equals("3"))
                File_From="leadersfri.csv";
            else
                File_From="leadersoffice.csv";
            FileWriter fileWriter = new FileWriter((getFilesDir() + File_From));

            //convert data to a string
            String s1 = Integer.toString(rec1.getScore());
            String s2 = Integer.toString(rec2.getScore());
            String s3 = Integer.toString(rec3.getScore());
            //read out data to csv file
            fileWriter.write("1,");
            fileWriter.write(s1);
            fileWriter.write(',');
            fileWriter.write(rec1.getName());
            fileWriter.write(',');
            fileWriter.write('\n');

            fileWriter.write("2,");
            fileWriter.write(s2);
            fileWriter.write(',');
            fileWriter.write(rec2.getName());
            fileWriter.write(',');
            fileWriter.write('\n');

            fileWriter.write("3,");
            fileWriter.write(s3);
            fileWriter.write(',');
            fileWriter.write(rec3.getName());
            fileWriter.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

        // end of code to update leaderbaord

        //sets up the textboxes to be used on the layout display
        TextView Rank1 = (TextView) findViewById(R.id.textView6);
        TextView Rank2 = (TextView) findViewById(R.id.textView8);
        TextView Rank3 = (TextView) findViewById(R.id.textView9);

        //outputs the scoreboard to the display
        String DisplayRank = "1 " + "| " + rec1.getScore() + " | " + rec1.getName();
        Rank1.setText(DisplayRank);

        DisplayRank = "2 " + "| " + rec2.getScore() + " | " + rec2.getName();
        Rank2.setText(DisplayRank);

        DisplayRank = "3 " + "| " + rec3.getScore() + " | " + rec3.getName();
        Rank3.setText(DisplayRank);

    }

    private List<LeaderBoardRecord> Ranks = new ArrayList<>();

    private void readLeaderBoard() throws IOException {

        String File_From;
        if(state.equals("1"))
            File_From="leaders.csv";
        else if(state.equals("2"))
            File_From="leadershimym.csv";
        else if(state.equals("3"))
            File_From="leadersfri.csv";
        else
            File_From="leadersoffice.csv";
        File file = new File((getFilesDir() + File_From));
        if (!file.exists()) {
            try {
                file.createNewFile();
                BufferedWriter writer = new BufferedWriter(new FileWriter(file));
                String data="1,0,Foo\n";
                writer.write(data);
                data="2,0,Bar\n";
                writer.write(data);
                data="3,0,Baz\n";
                writer.write(data);
                writer.close();

            }
            catch (IOException e) {
                e.printStackTrace();
            }
        }

        BufferedReader reader = new BufferedReader(new FileReader(file));
        String line = " ";
        try {

                while ((line = reader.readLine()) != null) {
                    String[] tokens = line.split(",");

                    LeaderBoardRecord rec = new LeaderBoardRecord();
                    rec.setRank(Integer.parseInt(tokens[0]));
                    rec.setScore(Integer.parseInt(tokens[1]));
                    rec.setName(tokens[2]);
                    Ranks.add(rec);

                    Log.d("TriviApp", "Just read " + rec);
                }
        } catch (IOException e) {
            Log.wtf("TriviApp", "Error reading rank file on line " + line, e);
            e.printStackTrace();
        } finally { // updated as reader was never closed
            reader.close();
        }
    }
}
