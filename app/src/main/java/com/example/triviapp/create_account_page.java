package com.example.triviapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class create_account_page extends AppCompatActivity {

    public EditText name;
    private EditText password;
    private EditText password_confirm;
    private Button createAccount;
    private TextView info;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_account_page);

        name = (EditText)findViewById(R.id.etCreateName);
        password = (EditText)findViewById(R.id.etCreatePassword);
        password_confirm = (EditText)findViewById(R.id.etCreatePassword_confirm);
        createAccount = (Button)findViewById(R.id.btnCreateAccount);
        info = (TextView)findViewById(R.id.tvCreateInfo);

        createAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validate(password.getText().toString(),  password_confirm.getText().toString());
            }
        });
    }

    private void validate(String userPassword, String userPassword_confirm){

        if(userPassword.equals(userPassword_confirm)){
            Intent intent = new Intent(create_account_page.this, login_page.class);
            startActivity(intent);
        }
        else{
            info.setText("Passwords Do Not Match");
            }

    }
}

