package com.example.triviapp;

import android.content.Intent;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Q_Page extends AppCompatActivity {

    public String answer;
    public static String state;
    public Button Option1,Option2,Option3,Option4,Correct;
    public boolean Flag;
    public int j=0,Score=0,Total=0;
    public static final String EXTRA_MESSAGE = "com.example.TriviApp.MESSAGE";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_q__page); ////////////////MAKE THIS GENERIC

        Intent intent = getIntent();
//        String message = intent.getStringExtra(Q_Page.EXTRA_MESSAGE);
//        String[] tokens = message.split(","); //sets the delimeter to a ','
//        //takes the score and total and parses them into ints stored in token[0-1]
//        int Score = Integer.parseInt(tokens[0]);
//        int Total = Integer.parseInt(tokens[1]);
//        Intent intent = getIntent();
        state = intent.getStringExtra(Q_Page.EXTRA_MESSAGE);

        try {
            readData();

        } catch (IOException e) {
            e.printStackTrace();
        }

        int size=Data_array.size();

        ArrayList<Integer> list = new ArrayList<>();

        for (int i=0; i<size; i++) {
            list.add(i);
        }

        Collections.shuffle(list);  //// RANDOMIZE THIS

        int i=list.get(j);
        Load(i);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.example_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case  R.id.item1:
                Toast.makeText(this, "Item 1 selected", Toast.LENGTH_SHORT).show();
                return true;

            case  R.id.item2:
                Toast.makeText(this, "Item 2 selected", Toast.LENGTH_SHORT).show();
                return true;

            case  R.id.item3:
                Toast.makeText(this, "Item 3 selected", Toast.LENGTH_SHORT).show();
                return true;

            case  R.id.subitem1:
                Toast.makeText(this, "Sub Item 1 selected", Toast.LENGTH_SHORT).show();
                return true;

            case  R.id.subitem2:
                Toast.makeText(this, "Sub Item 2 selected", Toast.LENGTH_SHORT).show();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }

    }


    public void Load(int i)
    {
        TextView Score_Total = (TextView)findViewById(R.id.scoreText);
        String S = Integer.toString(Score);
        String T = Integer.toString(Total);
        String Final ="Score: "+ S +" | Total: "+ T ;
        Score_Total.setText(Final);
        TextView Question= (TextView)findViewById(R.id.questionText);
        String Q = Data_array.get(i).getQues();

        Question.setText(Q);


        ImageView Q_image=(ImageView) findViewById(R.id.imageView);


        int LOGO;
        if(state.equals("1"))
            LOGO=R.drawable.got_logo;
        else if(state.equals("2"))
            LOGO=R.drawable.himym;
        else if(state.equals("3"))
            LOGO=R.drawable.fri_logo;
        else if(state.equals("4"))
            LOGO=R.drawable.office_logo;
        //Should be a Simpsons logo
        else if(state.equals("5"))
            LOGO=R.drawable.simpsons_logo;
        else
            LOGO=R.drawable.got_logo;

        Q_image.setImageResource(LOGO);

        Option1=findViewById(R.id.choice1);
        Option2=findViewById(R.id.choice2);
        Option3=findViewById(R.id.choice3);
        Option4=findViewById(R.id.choice4);
        Option1.setBackgroundResource(R.drawable.sample);
        Option2.setBackgroundResource(R.drawable.sample);
        Option3.setBackgroundResource(R.drawable.sample);
        Option4.setBackgroundResource(R.drawable.sample);
        String O1=Data_array.get(i).getOp1();
        String O2=Data_array.get(i).getOp2();
        String O3=Data_array.get(i).getOp3();
        String O4=Data_array.get(i).getOp4();
        answer=Data_array.get(i).getAns();
        Option1.setText(O1);
        Option2.setText(O2);
        Option3.setText(O3);
        Option4.setText(O4);
        if (answer.equals(O1))
            Correct=Option1;
        else if (answer.equals(O2))
            Correct=Option2;
        else if (answer.equals(O3))
            Correct=Option3;
        else if (answer.equals(O4))
            Correct=Option4;
        Flag=false;
    }
    private List<QuestionRecord> Data_array =new ArrayList<>();

    private void readData() throws IOException {

        int res;
        if(state.equals("1"))
            res=R.raw.game_of_thrones;
        else if(state.equals("2"))
            res=R.raw.himymcsv;
        else if(state.equals("3"))
            res=R.raw.friends;
        else if(state.equals("4"))
            res=R.raw.office;
        else if(state.equals("5"))
           res=R.raw.simpsons;
        else
            res=R.raw.game_of_thrones;


        InputStream IS=getResources().openRawResource(res);
        BufferedReader reader= new BufferedReader(
                new InputStreamReader(IS, Charset.forName("UTF-8"))
        );

        String line=" ";
        try{
            while( (line=reader.readLine()) !=null) {
                //Split by ','
                String[] tokens=line.split(",");
                //Read the data
                QuestionRecord rec=new QuestionRecord();
                //rec.setImage(tokens[0]);
                rec.setQues(tokens[0]);
                rec.setOp1(tokens[1]);
                rec.setOp2(tokens[2]);
                rec.setOp3(tokens[3]);
                rec.setOp4(tokens[4]);
                rec.setAns(tokens[5]);
                Data_array.add(rec);

                Log.d("TriviApp","Just created "+ rec);
            }
        }
        catch (IOException e)
        {
            Log.wtf("TriviApp","Error reading data file on line "+ line,e);
            e.printStackTrace();
        }
    }

    public void next_Ques()
    {
        j++;
        if(j==10) //Ending questions
        {
            Intent intent1=new Intent(this, result_screen.class);
            String S=Integer.toString(Score);
            String T=Integer.toString(Total);
            String Final_Message= S + "," + T + "," + state ;

            intent1.putExtra(EXTRA_MESSAGE,Final_Message);
            startActivity(intent1);
        }
        else
            Load(j);
    }

    public void Click_option(View view)
    {
        j++;
        if(j==10)
        {
            Intent intent1=new Intent(this, NextPage.class);
            String S=Integer.toString(Score);
            String T=Integer.toString(Total);
            String Score_Message=S+","+T ;
            intent1.putExtra(EXTRA_MESSAGE,Score_Message);
            startActivity(intent1);
        }
        else if(j==5){
            //start popup for more betting
        }
        else {
            if(Flag==false)
                Total=Total+10;
            Load(j);
        }
    }

    public void Option1(View view) //throws InterruptedException {
    {
        if(Flag==false)
        {
            Total=Total+10;
            final String Optxt=Option1.getText().toString();
            if(Optxt.equals(answer))
            {
                Score=Score+10;
            }

            new CountDownTimer(1000, 500) {

                public void onTick(long millisUntilFinished)
                {
                    if (Optxt.equals(answer)) {
                        Option1.setBackgroundResource(R.drawable.right_button);
                    }
                    else {
                        Option1.setBackgroundResource(R.drawable.wrong_button);
                        Correct.setBackgroundResource(R.drawable.right_button);
                    }
                    Flag=true;
                }

                public void onFinish() {
                    next_Ques();
                }
            }.start();
        }
    }

    public void Option2(View view) //throws InterruptedException {
    {
        if(Flag==false)
        {
            Total=Total+10;
            final String Optxt=Option2.getText().toString();
            if(Optxt.equals(answer))
            {
                Score=Score+10;
            }
            new CountDownTimer(1000, 500) {

                public void onTick(long millisUntilFinished)
                {
                    if(Optxt.equals(answer))
                    {
                        Option2.setBackgroundResource(R.drawable.right_button);
                    }
                    else {
                        Option2.setBackgroundResource(R.drawable.wrong_button);
                        Correct.setBackgroundResource(R.drawable.right_button);
                    }
                    Flag=true;
                }

                public void onFinish() {
                    next_Ques();
                }
            }.start();
        }
    }

    public void Option3(View view) //throws InterruptedException {
    {
        if(Flag==false)
        {
            Total=Total+10;
            final String Optxt=Option3.getText().toString();
            if(Optxt.equals(answer))
            {
                Score=Score+10;
            }
            new CountDownTimer(1000, 500) {

                public void onTick(long millisUntilFinished) {
                    if(Optxt.equals(answer))
                    {
                        Option3.setBackgroundResource(R.drawable.right_button);
                    }
                    else {
                        Option3.setBackgroundResource(R.drawable.wrong_button);
                        Correct.setBackgroundResource(R.drawable.right_button);
                    }
                    Flag=true;
                }

                public void onFinish() {
                    next_Ques();
                }
            }.start();

        }
    }

    public void Option4(View view) //throws InterruptedException {
    {
        if(Flag==false)
        {
            Total=Total+10;
            final String Optxt=Option4.getText().toString();
            if(Optxt.equals(answer))
            {
                Score=Score+10;
            }
            new CountDownTimer(1000, 500) {

                public void onTick(long millisUntilFinished) {
                    if(Optxt.equals(answer))
                    {
                        Option4.setBackgroundResource(R.drawable.right_button);
                    }
                    else {
                        Option4.setBackgroundResource(R.drawable.wrong_button);
                        Correct.setBackgroundResource(R.drawable.right_button);
                    }
                    Flag=true;
                }

                public void onFinish() {
                    next_Ques();
                }
            }.start();
        }
    }

}