package com.example.triviapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

public class initial_betting_page extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_initial_betting_page);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.example_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case  R.id.item1:
                Toast.makeText(this, "Item 1 selected", Toast.LENGTH_SHORT).show();
                return true;

            case  R.id.item2:
                Toast.makeText(this, "Item 2 selected", Toast.LENGTH_SHORT).show();
                return true;

            case  R.id.item3:
                Toast.makeText(this, "Item 3 selected", Toast.LENGTH_SHORT).show();
                return true;

            case  R.id.subitem1:
                Toast.makeText(this, "Sub Item 1 selected", Toast.LENGTH_SHORT).show();
                return true;

            case  R.id.subitem2:
                Toast.makeText(this, "Sub Item 2 selected", Toast.LENGTH_SHORT).show();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }

    }


    //all just go to the question page for the moment no other functionality has been added

    public void coins_5(View view)
    {
        //check both players have required amount of coins
        //if yes bring them to next screen
        //if no popup "Not enough coins to play" or something like that
        //the name of the account with too few coins would be cool too

        Intent intent = new Intent(this, player_search.class);
        startActivity(intent);
    }

    public void coins_10(View view)
    {
        //check both players have required amount of coins
        //if yes bring them to next screen
        //if no popup "Not enough coins to play" or something like that
        //the name of the account with too few coins would be cool too

        Intent intent = new Intent(this, player_search.class);
        startActivity(intent);
    }

    public void coins_15(View view)
    {
        //check both players have required amount of coins
        //if yes bring them to next screen
        //if no popup "Not enough coins to play" or something like that
        //the name of the account with too few coins would be cool too

        Intent intent = new Intent(this, player_search.class);
        startActivity(intent);
    }

    public void coins_20(View view)
    {
        //check both players have required amount of coins
        //if yes bring them to next screen
        //if no popup "Not enough coins to play" or something like that
        //the name of the account with too few coins would be cool too

        Intent intent = new Intent(this, player_search.class);
        startActivity(intent);
    }
}
