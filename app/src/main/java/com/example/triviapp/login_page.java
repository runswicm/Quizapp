package com.example.triviapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class login_page extends AppCompatActivity {


    public EditText name;
    private EditText password;
    private TextView info;
    private Button login;
    private Button createAccount;
    private int  counter = 5;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_page);

        name = (EditText)findViewById(R.id.etCreateName);
        password = (EditText)findViewById(R.id.etPassword);
        info = (TextView)findViewById(R.id.tvInfo);
        login = (Button)findViewById(R.id.btnCreateAccount);
        createAccount = (Button)findViewById(R.id.go_to_create_account);

        info.setText("Number of attempts remaining: 5");

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validate(name.getText().toString(),  password.getText().toString());
            }
        });

        createAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(login_page.this, create_account_page.class);
                startActivity(intent);
            }
        });



    }





    private void validate(String userName, String userPassword){

        Intent intent = new Intent(login_page.this, Home_page.class);
        startActivity(intent);

        // comment in to enable authentication

//         if(userName.equals("admin")  && userPassword.equals("1234")){
//
//             Intent intent = new Intent(login_page.this, Home_page.class);
//
//             startActivity(intent);
//         }
//         else{
//              counter--;
//
//              info.setText("Number of attempts remaining: " + String.valueOf(counter));
//              if(counter == 0){
//                  login.setEnabled(false);
//              }
//         }
    }
}
